import { Fragment } from 'react';
import { Container } from 'react-bootstrap'
import AppNavBar from './components/AppNavBar';

import ProductView from './components/ProductView'
import Products from './pages/Products'
import Home from './pages/Home'
import Register from './pages/Register'
import Login from './pages/Login'
import Logout from './pages/Logout'
import Error from './pages/Error'
import AdminPanel from './pages/AdminPanel'

import { useState, useEffect } from 'react'

import { UserProvider } from './UserContext'

import './App.css';

import {BrowserRouter, Route, Routes} from 'react-router-dom';


function App() {
  
  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })


  const unsetUser =() =>{
    localStorage.clear()
  }

  useEffect(() =>{
      fetch(`${process.env.REACT_APP_API_URL}/users/details`,{
        headers : {
          Authorization : `Bearer ${localStorage.getItem('token')}`
        }
      })
      .then(response => response.json())
      .then(data =>{
        setUser({
          id: data._id,
          isAdmin : data.isAdmin
        })
      })
  },[])


  return (
    <UserProvider value = {{user, setUser, unsetUser}}>
      <BrowserRouter>
        <AppNavBar />
          <Container>
            <Routes>
              <Route path='/' element = {<Home/>}/>
              <Route path='/products' element = {<Products/>}/>
              <Route path='/register' element = {<Register/>}/>
              <Route path='/login' element = {<Login/>}/>
              <Route path='/logout' element = {<Logout/>}/>
              <Route path='/products/:id' element = {<ProductView/>}/>
              <Route path= '/admin' element = {<AdminPanel/>}/>
              <Route path='*' element = {<Error/>}/>
            </Routes>
          </Container>
      </BrowserRouter>
    </UserProvider>
  );
}


export default App;

