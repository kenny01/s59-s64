import { Fragment } from 'react';
import Banner  from '../components/Banner';
import Highlights  from '../components/Highlights';

export default function Home (){
	return (
		<Fragment>
			<div className="home-container">
				<Banner className ="banner" />
			<div className="highlights-container">
				<Highlights />
			</div>
			</div>
		</Fragment>

	)
}