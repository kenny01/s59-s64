// It will allow us to navigate from one page of our application to another page
import {Navigate} from 'react-router-dom'
import {useContext, useEffect} from 'react'
import UserContext from '../UserContext'

export default function Logout(){
	// localStorage.clear()
	const {unsetUser, setUser} = useContext(UserContext)

	useEffect(()=> {
		unsetUser()
		setUser({
			id: null,
			isAdmin : null
		})
	},[])

	return (
		<Navigate to = '/login'/>
	)

}