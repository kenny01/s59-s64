import { Col , Row, Card, Button } from 'react-bootstrap';
import PropTypes from 'prop-types';
import { useState, useEffect } from 'react';
import "../App.css"
import {Link} from 'react-router-dom'

export default function ProductCard({productProp}) {
  // Checks to see if the data was successfully pased
  // console.log(courseProp);
  
 const { _id, name, description, price, image } = productProp;

 return (
 
     <Row className= 'mt-3 mb-3'>
        <Col style = {{ marginRight:'50px'}} className="d-flex">
            <Card  className = "mx-auto" style={{width: '300px',  minHeight: '100%'}} >
            <Card.Img className = "image img-fluid mx-auto d-block p-3" variant="top" src={image} alt={name} />
            <Card.Body className ="text-center d-flex flex-column">
            <Card.Title>{name}</Card.Title>
            <Card.Subtitle>Description:</Card.Subtitle>
            <Card.Text>{description}</Card.Text>
            <div className ="text-center mt-auto">
            <Card.Subtitle>Price:</Card.Subtitle>
            <Card.Text>{`₱ ${price}`}</Card.Text>
            <Button as={Link} to={`/products/${_id}`} variant="primary">
             Details
            </Button>  
            </div>
      </Card.Body>
    </Card>
    </Col>
    </Row>
   

  )
}
ProductCard.propTypes = {
  productProp: PropTypes.shape({
    _id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
  }),
};

