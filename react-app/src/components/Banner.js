import { Button, Row, Col } from 'react-bootstrap'
 import { Link } from 'react-router-dom'

export default function Banner(){
	return (
		<Row>
		  <Col className="p-5 text-center">
		      <h1>Kenny Online Vape Shop </h1>
		      <p>This is where you can find your vaping needs!</p>
		       <Button as = {Link} to = "/products" variant="primary">Order Now!</Button>
		   </Col>
		 </Row>
	)
}